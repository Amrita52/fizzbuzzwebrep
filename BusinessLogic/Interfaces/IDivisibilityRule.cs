﻿using System.Collections.Generic;

namespace BusinessLogic
{
    public interface IDivisibilityRule
    {
        IList<string> DivisibilityRule(int input);
    }

}
