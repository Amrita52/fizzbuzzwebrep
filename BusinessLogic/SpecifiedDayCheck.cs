﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public class SpecifiedDayCheck : ISpecifiedDayCheck
    {
        public bool IsSpecificDayIsWednesday(DayOfWeek day)
        {
            return day == DayOfWeek.Wednesday;
        }

    }

}
